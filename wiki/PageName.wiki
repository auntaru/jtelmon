#summary Ubuntu NetBeans Subversion.
#labels Featured

= Introduction =

Getting-netbeans-to-work-with-subversion-on-ubuntu:

0.1     apt-get install subversion
0.2     locate svn;  which svn; 

= Details =

Here’s how I got Ubuntu.NetBeans to work with (code.google.com) Subversion:

   1. Browse to 
          Tools -> Options -> Miscellaneous -> Versioning -> Subversion
   2. Fill in the “Specify SVN Home Folder” field with “/usr/bin”, without the quotes.
   3. Restart NetBeans.