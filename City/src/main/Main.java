/*
 *
 * Object Database Programming with JPA and NetBeans
 * http://java.dzone.com/articles/getting-started-jpa-and
 * http://www.objectdb.com/object/db/database/purchase
 * ObjectDB License costs $500
 * You may use ObjectDB for free under the restriction
 * of maximum 10 entity classes
 * and one million entity objects per database file
 * (no time limit).
 * ObjectDB is a commercial software.
 * http://www.objectdb.com/tutorial/jpa/netbeans
 * http://www.objectdb.com/java/jpa
 * 
 */


package main;

import gui.frmCity;

public class Main {

    public static void main(String[] args) {
        frmCity city = new frmCity();
        city.setVisible(true);
    }

}
